# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from django.views.generic import FormView, TemplateView
from django.conf import settings
from django.template.loader import get_template
from django.core.mail import EmailMultiAlternatives
from django.http import HttpResponse
from django.http import JsonResponse

from .forms import EnquiryForm
from .models import ContactEmail


class ContactView(FormView):

    form_class = EnquiryForm
    template_name = "contact/contact.html"

    def form_valid(self, form):
        print(self.request.POST)
        text_template = get_template('email_templates/contact_message.html')
        user_name = form.cleaned_data['name']
        subject = 'Contact request by {}'.format(user_name)
        context = {'formentry': self.request.POST}
        text_content = text_template.render(context)
        from_email = settings.EMAIL_HOST_USER
        to = [ContactEmail.get_solo()]
        msg = EmailMultiAlternatives(subject, text_content, from_email, to)
        msg.attach_alternative(text_content, "text/html")
        msg.send()
        form.save()
        return JsonResponse({'status':'success'})

    def form_invalid(self, form):
        return HttpResponse(json.dumps({'result': {'status': 'form_error', 'errors': form.errors}}),
                            content_type='application/json')

