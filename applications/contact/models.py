# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from solo.models import SingletonModel


class Enquiry(models.Model):
    name = models.CharField(max_length=235)
    email = models.EmailField()
    message = models.TextField()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Enquiry'
        verbose_name_plural = 'Enquiries'


class ContactEmail(SingletonModel):
    email = models.EmailField()

    def __str__(self):
        return self.email
