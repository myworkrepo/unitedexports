from __future__ import unicode_literals

from django.conf.urls import url

from django.contrib.auth.decorators import login_required

from .views import ContactView

urlpatterns = [
    url(r'^$', (ContactView.as_view()), name='contact'),
]
