# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel


class About(SingletonModel):
    vision_en = RichTextUploadingField()
    vision_ar = RichTextUploadingField()
    mission_en = RichTextUploadingField()
    mission_ar = RichTextUploadingField()
    image1 = models.ImageField(null=True, blank=True)
    name1 = models.CharField(max_length=255, null=True, blank=True)
    name1_ar = models.CharField(max_length=255, null=True, blank=True)
    designation1 = models.CharField(max_length=255, null=True, blank=True)
    designation1_ar = models.CharField(max_length=255, null=True, blank=True)
    image2 = models.ImageField(null=True, blank=True)
    name2 = models.CharField(max_length=255, null=True, blank=True)
    name2_ar = models.CharField(max_length=255, null=True, blank=True)
    designation2 = models.CharField(max_length=255, null=True, blank=True)
    designation2_ar = models.CharField(max_length=255, null=True, blank=True)
    image3 = models.ImageField(null=True, blank=True)
    name3 = models.CharField(max_length=255, null=True, blank=True)
    name3_ar = models.CharField(max_length=255, null=True, blank=True)
    designation3 = models.CharField(max_length=255, null=True, blank=True)
    designation3_ar = models.CharField(max_length=255, null=True, blank=True)
    image4 = models.ImageField(null=True, blank=True)
    name4 = models.CharField(max_length=255, null=True, blank=True)
    name4_ar = models.CharField(max_length=255, null=True, blank=True)
    designation4 = models.CharField(max_length=255, null=True, blank=True)
    designation4_ar = models.CharField(max_length=255, null=True, blank=True)
    image5 = models.ImageField(null=True, blank=True)
    name5 = models.CharField(max_length=255, null=True, blank=True)
    name5_ar = models.CharField(max_length=255, null=True, blank=True)
    designation5 = models.CharField(max_length=255, null=True, blank=True)
    designation5_ar = models.CharField(max_length=255, null=True, blank=True)
    image6 = models.ImageField(null=True, blank=True)
    name6 = models.CharField(max_length=255, null=True, blank=True)
    name6_ar = models.CharField(max_length=255, null=True, blank=True)
    designation6 = models.CharField(max_length=255, null=True, blank=True)
    designation6_ar = models.CharField(max_length=255, null=True, blank=True)
    image7 = models.ImageField(null=True, blank=True)
    name7 = models.CharField(max_length=255, null=True, blank=True)
    name7_ar = models.CharField(max_length=255, null=True, blank=True)
    designation7 = models.CharField(max_length=255, null=True, blank=True)
    designation7_ar = models.CharField(max_length=255, null=True, blank=True)

    def __str__(self):
        return "About"

    class Meta:
        verbose_name = 'About'
        verbose_name_plural = 'About'