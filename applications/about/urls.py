from __future__ import unicode_literals

from django.conf.urls import url

from django.contrib.auth.decorators import login_required

from .views import AboutView

urlpatterns = [
    url(r'^$', (AboutView.as_view()), name='about'),
]
