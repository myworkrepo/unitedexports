# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.utils.translation import ugettext_lazy as _
from solo.models import SingletonModel


class Category(models.Model):
    category_name_en = models.CharField(max_length=255)
    category_name_ar = models.CharField(max_length=255)
    category_image = models.ImageField(null=True, blank=True)
    publish = models.BooleanField(default=True)
    created = models.DateField(auto_now=True)

    def __str__(self):
        return self.category_name_en

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")


class Subcategory(models.Model):
    category = models.ForeignKey(Category, related_name='sub_category')
    sub_category_name_en = models.CharField(max_length=255)
    sub_category_name_ar = models.CharField(max_length=255)
    publish = models.BooleanField(default=True)
    created = models.DateField(auto_now=True)

    def __str__(self):
        return self.sub_category_name_en

    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Categories")


class Product(models.Model):
    product_name_en = models.CharField(max_length=255)
    product_name_ar = models.CharField(max_length=255)
    category = models.ForeignKey(Category)
    sub_category = models.ForeignKey(Subcategory, null=True, blank=True)
    price_dollar = models.FloatField()
    price_other = models.FloatField()
    offer_price = models.FloatField(null=True, blank=True)
    publish = models.BooleanField(default=True)
    created = models.DateField(auto_now=True)
    is_trending = models.BooleanField(default=False)

    def __str__(self):
        return self.product_name_en

    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Products")


class ProductImage(models.Model):
    product = models.ForeignKey(Product, related_name='images')
    image_title = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField()
    created = models.DateField(auto_now=True)

    def __str__(self):
        return self.image.url

    class Meta:
        verbose_name = _("Image")
        verbose_name_plural = _("Images")


class ProductPageBanner(SingletonModel):
    image_title = models.CharField(max_length=255, null=True, blank=True)
    image = models.ImageField()

    def __str__(self):
        return "Product Page Banner"

    class Meta:
        verbose_name = 'Product Page Banner'
        verbose_name_plural = 'Product Page Banner'
