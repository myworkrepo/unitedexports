# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from solo.admin import SingletonModelAdmin

from .models import Product, ProductImage, Category, Subcategory, ProductPageBanner


class ProductImageInline(admin.TabularInline):
    model = ProductImage


class CategoryInline(admin.TabularInline):
    model = Subcategory


class ProductsAdmin(admin.ModelAdmin):
    list_display = ('product_name_en', 'category', 'is_trending')
    list_editable = ['is_trending']
    inlines = [ProductImageInline,]


class CategoryAdmin(admin.ModelAdmin):
    inlines = [CategoryInline,]

admin.site.register(Product, ProductsAdmin)
admin.site.register(Category, CategoryAdmin)
admin.site.register(ProductPageBanner, SingletonModelAdmin)
