# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from django.core.paginator import Paginator

from .models import Product, Category, ProductPageBanner


class ProductView(ListView):
    template_name = 'product/product.html'  # Default: <app_label>/<model_name>_list.html
    context_object_name = 'all_products'  # Default: object_list
    paginate_by = 15
    queryset = Product.objects.all()

    def get_queryset(self):
        queryset = Product.objects.all()
        if self.request.GET.get('category'):
            category = self.request.GET.get('category')
            if not category == 'all':
                queryset = Product.objects.filter(category__category_name_en=category)
                # context['all_products'] = products
        if self.request.GET.get('sub_category'):
            sub_category = self.request.GET.get('sub_category')
            queryset = Product.objects.filter(sub_category__sub_category_name_en=sub_category)
        return queryset

    def get_context_data(self, **kwargs):
        context = super(ProductView, self).get_context_data(**kwargs)
        context['categories'] = Category.objects.all()
        context['banner'] = ProductPageBanner.get_solo()
        if self.request.GET.get('currency'):
            if self.request.GET.get('currency') == 'Qatar Riyal':
                context['riyal'] = True
        return context