from .models import SocialLinks


def social_links(request):
    links = SocialLinks.get_solo()
    return {'links': links}
