# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from solo.admin import SingletonModelAdmin

from .models import HomepageThreeImagesSection, HomepageFirstRowImages, HomepageBottomImages, \
    HomepageBanner, HomepageTwoImagesSection, SocialLinks


admin.site.register(HomepageBanner, admin.ModelAdmin)
admin.site.register(HomepageBottomImages, admin.ModelAdmin)
admin.site.register(HomepageFirstRowImages, admin.ModelAdmin)
admin.site.register(SocialLinks, SingletonModelAdmin)
admin.site.register(HomepageTwoImagesSection, admin.ModelAdmin)
# Register your models here.
