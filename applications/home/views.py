# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.views.generic import TemplateView

from applications.product.models import Product
from .models import HomepageBanner, HomepageBottomImages, HomepageFirstRowImages, \
    HomepageThreeImagesSection, HomepageTwoImagesSection


class HomeView(TemplateView):
    template_name = 'home/index.html'

    def get_context_data(self, *args, **kwargs):
        context = super(HomeView, self).get_context_data(*args, **kwargs)
        context['best_selling'] = Product.objects.all()
        context['trending'] = Product.objects.filter(is_trending=True)
        context['banners'] = HomepageBanner.objects.all()
        context['bottoms'] = HomepageBottomImages.objects.all()
        context['first_row'] = HomepageFirstRowImages.objects.all()[:5]
        context['three_images'] = HomepageThreeImagesSection.objects.all()[:3]
        context['two_images'] = HomepageTwoImagesSection.objects.all()[:2]
        if self.request.GET.get('currency'):
            if self.request.GET.get('currency') == 'Qatar Riyal':
                context['riyal'] = True
        return context