# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from ckeditor_uploader.fields import RichTextUploadingField
from solo.models import SingletonModel

from applications.product.models import Category


class HomepageBanner(models.Model):
    image_title = models.CharField(max_length=255)
    banner_image = models.ImageField(help_text="image size 1090*450")
    category = models.ForeignKey(Category, null=True, blank=True)
    order = models.PositiveIntegerField(default=0)
    publish = models.BooleanField(default=True)

    def __str__(self):
        return self.image_title

    class Meta:
        verbose_name = 'Banner Image'
        verbose_name_plural = 'Banner Images'
        ordering = ['order']


class HomepageFirstRowImages(models.Model):
    image_title = models.CharField(max_length=255)
    image = models.ImageField(help_text="image size 250*270")
    order = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, null=True, blank=True)
    publish = models.BooleanField(default=True)

    def __str__(self):
        return self.image_title

    class Meta:
        verbose_name = 'Homepage First Row Image'
        verbose_name_plural = 'Homepage First Row Images'
        ordering = ['order']


class HomepageThreeImagesSection(models.Model):
    image_title = models.CharField(max_length=255)
    image = models.ImageField(null=True, blank=True)
    order = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, null=True, blank=True)
    publish = models.BooleanField(default=True)

    def __str__(self):
        return self.image_title

    class Meta:
        verbose_name = 'Homepage ThreeImages Section'
        verbose_name_plural = 'Homepage ThreeImages Section'
        ordering = ['order']


class HomepageTwoImagesSection(models.Model):
    image_title = models.CharField(max_length=255)
    image = models.ImageField(null=True, blank=True)
    category = models.ForeignKey(Category, null=True, blank=True)
    order = models.PositiveIntegerField(default=0)
    publish = models.BooleanField(default=True)

    def __str__(self):
        return self.image_title

    class Meta:
        verbose_name = 'Homepage Two Images Section'
        verbose_name_plural = 'Homepage Two Images Section'
        ordering = ['order']


class HomepageBottomImages(models.Model):
    image_title = models.CharField(max_length=255)
    image = models.ImageField(help_text="image size 140*73")
    order = models.PositiveIntegerField(default=0)
    category = models.ForeignKey(Category, null=True, blank=True)
    publish = models.BooleanField(default=True)

    def __str__(self):
        return self.image_title

    class Meta:
        verbose_name = 'Homepage Bottom Image'
        verbose_name_plural = 'Homepage Bottom Images'
        ordering = ['order']


class SocialLinks(SingletonModel):
    facebook = models.URLField()
    instagram = models.URLField()
    twitter = models.URLField()

    def __str__(self):
        return "Social Links"

    class Meta:
        verbose_name = 'Social Links'
        verbose_name_plural = 'Social Links'
