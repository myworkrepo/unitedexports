
DATABASES = {
   'default': {
       'ENGINE': 'django.db.backends.mysql',
       'NAME': 'exports',
       'USER':'root',
       'PASSWORD': 'myserverdb',
       'HOST':'localhost',
       'OPTIONS': {
           'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
       }
   }
}
